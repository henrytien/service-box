cmake_minimum_required(VERSION 3.5)

project(service-box-unittest)

#设置包含路径以及安装时候的路径
target_include_directories(service_box_unittest
	PUBLIC
	${PROJECT_SOURCE_DIR}
	${PROJECT_SOURCE_DIR}/../../src
    ${PROJECT_SOURCE_DIR}/../../thirdparty/include
    ${PROJECT_SOURCE_DIR}/../../thirdparty/root
    #cross-platform
    $<$<PLATFORM_ID:Linux>: /usr/include/>
	$<$<PLATFORM_ID:Linux>: /usr/local/include/>
)

target_compile_options(service_box_unittest PUBLIC 
	$<$<PLATFORM_ID:Linux>: -m64 -g -Wall -Wno-unused-variable -fnon-call-exceptions -fPIC>
	$<$<AND:$<PLATFORM_ID:Linux>,$<COMPILE_LANGUAGE:CXX>>:-fpermissive>
	$<$<PLATFORM_ID:Windows>: /wd4706 /wd4206 /wd4100 /wd4244 /wd4127 /wd4702 /wd4324 /wd4310 /W4 /MP /EHa>  #/permissive-
    )
target_compile_features(service_box_unittest PUBLIC cxx_std_17)

target_compile_definitions(service_box_unittest PUBLIC
    USE_STATIC_LIB THREADED CURL_STATICLIB
    $<$<AND:$<PLATFORM_ID:Windows>,$<CONFIG:Debug>>:  _WINSOCK_DEPRECATED_NO_WARNINGS WIN32 _WINDOWS _DEBUG _CONSOLE _CRT_SECURE_NO_WARNINGS WIN32_LEAN_AND_MEAN>
    $<$<AND:$<PLATFORM_ID:Windows>,$<CONFIG:Release>>: _WINSOCK_DEPRECATED_NO_WARNINGS _CRT_SECURE_NO_WARNINGS WIN32_LEAN_AND_MEAN WIN32 _WINDOWS NDEBUG>
)

SET(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/lib)

link_directories(${PROJECT_SOURCE_DIR}/../../thirdparty/lib/linux)
aux_source_directory(. SRC_FILES)
aux_source_directory(${PROJECT_SOURCE_DIR}/../framework SRC_FILES)

link_directories(${PROJECT_SOURCE_DIR}/../../src/repo/lib/root)
link_directories(${PROJECT_SOURCE_DIR}/../../src/repo/lib/proxy/example/ServiceDynamic)
link_directories(${PROJECT_SOURCE_DIR}/../../src/repo/lib/example)
link_directories(${PROJECT_SOURCE_DIR}/../../src/repo/lib/stub/example/ServiceDynamic)
link_directories(${PROJECT_SOURCE_DIR}/../../src/lib)

add_executable (
	service_box_unittest
	${SRC_FILES}    
)

target_link_libraries(service_box_unittest PUBLIC
    -Wl,--whole-archive libservice_box.a -Wl,--no-whole-archive
    librpc.a
    "$<$<AND:$<PLATFORM_ID:Windows>,$<CONFIG:Debug>>: ${PROJECT_SOURCE_DIR}/../../src/repo/thirdparty/lib/windows/debug/libprotobufd.lib>"
    "$<$<AND:$<PLATFORM_ID:Windows>,$<CONFIG:Release>>: ${PROJECT_SOURCE_DIR}/../../src/repo/thirdparty/lib/windows/release/libprotobuf.lib>"
    "$<$<PLATFORM_ID:Linux>:${PROJECT_SOURCE_DIR}/../../src/repo/thirdparty/lib/linux/libprotobuf.a>"
    libzookeeper.a
    libhashtable.a
    libhiredis.a
    libcurl.a
    libz.a
    -Wl,--whole-archive libexample.a libServiceDynamic_proxy.a libServiceDynamic_stub.a -Wl,--no-whole-archive
    -lpthread
    -ldl
)
