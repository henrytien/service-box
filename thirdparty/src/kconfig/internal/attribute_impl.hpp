/*
 * Copyright (c) 2013-2015, dennis wang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ATTRIBUTE_IMPL_HPP
#define ATTRIBUTE_IMPL_HPP

#include "kconfig/interface/config.h"

using namespace kconfig;

template <typename T, int type> class AttributeImpl : public T {
public:
  AttributeImpl() {}
  virtual ~AttributeImpl() {}

  virtual bool isNumber() { return (Attribute::NUMBER == type); }

  virtual bool isString() { return (Attribute::STRING == type); }

  virtual bool isArray() { return (Attribute::ARRAY == type); }

  virtual bool isTable() { return (Attribute::TABLE == type); }

  virtual bool isZone() { return (Attribute::ZONE == type); }

  virtual bool isBool() { return (Attribute::BOOL == type); }

  virtual bool isNull() { return (Attribute::NULL_VAL == type); }

  virtual int getType() override { return type; }

  virtual NumberAttribute *number() {
    if (isNumber()) {
      return dynamic_cast<NumberAttribute *>(this);
    }
    throw ConfigException("invalid type conversion");
  }

  virtual StringAttribute *string() {
    if (isString()) {
      return dynamic_cast<StringAttribute *>(this);
    }
    throw ConfigException("invalid type conversion");
  }

  virtual ArrayAttribute *array() {
    if (isArray()) {
      return dynamic_cast<ArrayAttribute *>(this);
    }
    throw ConfigException("invalid type conversion");
  }

  virtual TableAttribute *table() {
    if (isTable()) {
      return dynamic_cast<TableAttribute *>(this);
    }
    throw ConfigException("invalid type conversion");
  }

  virtual ZoneAttribute *zone() {
    if (isZone()) {
      return dynamic_cast<ZoneAttribute *>(this);
    }
    throw ConfigException("invalid type conversion");
  }

  virtual BoolAttribute *boolean() {
    if (isBool()) {
      return dynamic_cast<BoolAttribute *>(this);
    }
    throw ConfigException("invalid type conversion");
  }
};

#endif // ATTRIBUTE_IMPL_HPP
