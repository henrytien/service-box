﻿#include "load_balancer/load_balance.h"

namespace kratos {
namespace loadbalance {

class IPHASHSBalancer : public ILoadBalancer {
public:
  IPHASHSBalancer() = default;
  virtual ~IPHASHSBalancer() = default;

public:
  virtual auto add_lb_node(LoadBalanceNodeWeakPtr lbnode) -> bool override;
  virtual auto get_next(const std::string &ip = "")
      -> LoadBalanceNodeWeakPtr override;
  virtual auto clear() -> void override;
  virtual auto reset_balancer() -> void override{};

  static auto get_name() -> const std::string { return "IP_HASH_SCHEDULING"; };
  static auto get_mod() -> BalancerMod {
    return BalancerMod::Ip_Hash_Scheduling;
  }
  static auto creator() -> std::unique_ptr<ILoadBalancer> {
    return std::make_unique<IPHASHSBalancer>();
  }

private:
  static bool ip_hash_register_;
  std::int32_t server_count_{0};
  std::vector<LoadBalanceNodeWeakPtr> nodes_;
};

auto IPHASHSBalancer::add_lb_node(LoadBalanceNodeWeakPtr lbnode) -> bool {
  if (lbnode.expired()) {
    return false;
  }
  server_count_++;
  nodes_.emplace_back(lbnode);
  return true;
}

auto IPHASHSBalancer::get_next(const std::string &ip)
    -> LoadBalanceNodeWeakPtr {
  if (server_count_ == 0) {
    return LoadBalanceNodeWeakPtr();
  }
  std::size_t hash_code = std::hash<std::string>{}(ip);
  auto index = hash_code % server_count_;
  return nodes_[index];
}

auto IPHASHSBalancer::clear() -> void {
  nodes_.clear();
  server_count_ = 0;
  return;
}

Registe_Balancer(IPHASHSBalancer, ip_hash_, BalancerMod::Ip_Hash_Scheduling,
                 IPHASHSBalancer::creator)

} // namespace loadbalance
} // namespace kratos
