#include "kcp_loop.hh"
#include "box/box_network.hh"
#include "box/box_network_event.hh"
#include "util/object_pool.hh"
#include "util/time_util.hh"
#include <chrono>
#include <cstring>
#include <thread>

namespace kratos {
namespace loop {

KcpLoop::KcpLoop(service::BoxNetwork *network) { network_ = network; }

KcpLoop::~KcpLoop() {}

auto KcpLoop::start() -> bool {
  loop_ = kratos::make_shared_pool_ptr<network::UdpChannelLoop>();
  return true;
}

auto KcpLoop::stop() -> bool { return true; }

auto KcpLoop::worker_update() -> void {
  std::this_thread::sleep_for(std::chrono::milliseconds(10));
  auto current = util::get_os_time_millionsecond();
  // 更新所有KCP
  for (auto it : channelMap_) {
    ikcp_update(it.second->kcp_, (IUINT32)current);
  }
  loop_->update(std::bind(&KcpLoop::on_accept, this, std::placeholders::_1,
                          std::placeholders::_2),
                std::bind(&KcpLoop::on_receive, this, std::placeholders::_1,
                          std::placeholders::_2, std::placeholders::_3));
}

inline KcpLoop::KcpChannel::KcpChannel(network::UdpChannelLoop *loop,
                                       std::uint64_t channel) {
  loop_ = loop;
  channel_ = channel;
  kcp_ = ikcp_create(KCPCONV, this);
  ikcp_setoutput(kcp_, &KcpChannel::output);
  // 急速模式, 官方推荐
  ikcp_nodelay(kcp_, 1, 10, 2, 1);
  // 设置MTU
  ikcp_setmtu(kcp_, 548);
}

inline KcpLoop::KcpChannel::~KcpChannel() { ikcp_release(kcp_); }

int KcpLoop::KcpChannel::output(const char *buf, int len, ikcpcb *kcp,
                                void *user) {
  auto channel = reinterpret_cast<KcpChannel *>(user);
  auto id = channel->channel_;
  channel->loop_->send(id, buf, len);
  return 0;
}

auto KcpLoop::do_worker_event(const service::NetEventData &event_data) -> void {
  if (!loop_) {
    return;
  }
  switch (event_data.event_id) {
  case service::NetEvent::listen_request: {
    do_listen_request(event_data);
  } break;
  case service::NetEvent::connect_request: {
    do_connect_request(event_data);
  } break;
  case service::NetEvent::send_data_notify: {
    do_send_request(event_data);
  } break;
  case service::NetEvent::close_request: {
    do_close_request(event_data);
  } break;
  default:
    break;
  }
}

auto KcpLoop::get_network() -> service::BoxNetwork * { return network_; }

auto KcpLoop::do_listen_request(const service::NetEventData &event_data)
    -> void {
  auto channel_id = loop_->newAcceptor(
      event_data.listen_request.host,
      (std::uint16_t)event_data.listen_request.port,
      event_data.listen_request.name, NETWORK_IOLOOP_UDP_RECV_BUFFER_MAX_SIZE);
  service::NetEventData response{};
  response.event_id = service::NetEvent::listen_response;
  auto length =
      static_cast<int>(std::strlen(event_data.listen_request.name) + 1);
  response.listen_response.name = service::box_malloc(length);
  memcpy(response.listen_response.name, event_data.listen_request.name, length);
  if (!channel_id) {
    response.listen_response.channel_id = 0;
    response.listen_response.success = false;
  } else {
    response.listen_response.channel_id = channel_id;
    response.listen_response.success = true;
  }
  if (!get_network()->get_net_queue().send(response)) {
    // 发送失败，清理资源
    // 发生这种情况，会导致逻辑线程无法得知监听器是否启动成功
    response.clear();
    loop_->close(channel_id);
  } else {
    if (channel_id) {
      get_network()->get_listener_name_map()[channel_id] = {
          channel_id, event_data.listen_request.name};
      channelMap_[channel_id] =
          kratos::make_shared_pool_ptr<KcpChannel>(loop_.get(), channel_id);
    }
  }
}

auto KcpLoop::do_connect_request(const service::NetEventData &event_data)
    -> void {
  auto channel_id =
      loop_->newConnector(event_data.connect_request.host,
                          (std::uint16_t)event_data.connect_request.port,
                          NETWORK_IOLOOP_UDP_RECV_BUFFER_MAX_SIZE);
  service::NetEventData response{};
  response.event_id = service::NetEvent::connect_response;
  auto length = std::strlen(event_data.connect_request.name) + 1;
  response.connect_response.name = service::box_malloc(length);
  memset(response.connect_response.name, 0, length);
  memcpy(response.connect_response.name, event_data.connect_request.name,
         length);
  if (!channel_id) {
    response.connect_response.channel_id = 0;
    response.connect_response.success = false;
  } else {
    response.connect_response.channel_id = channel_id;
    response.connect_response.success = true;
  }
  if (!get_network()->get_net_queue().send(response)) {
    // 发送失败，清理资源
    response.clear();
    loop_->close(channel_id);
    return;
  } else {
    if (channel_id) {
      channelMap_[channel_id] =
          kratos::make_shared_pool_ptr<KcpChannel>(loop_.get(), channel_id);
    }
  }
}

auto KcpLoop::do_send_request(const service::NetEventData &event_data) -> void {
  auto it = channelMap_.find(event_data.send_data_notify.channel_id);
  if (it == channelMap_.end()) {
    return;
  }
  // NOTICE KCP单个包是有大小限制的, 需要修改源码才能更改
  if (ikcp_send(it->second->kcp_, event_data.send_data_notify.data_ptr,
                (int)event_data.send_data_notify.length)) {
    loop_->close(event_data.send_data_notify.channel_id);
    // 发送到逻辑线程
    service::NetEventData notify_data{};
    notify_data.event_id = service::NetEvent::close_notify;
    notify_data.close_notify.channel_id =
        event_data.send_data_notify.channel_id;
    if (!get_network()->get_net_queue().send(notify_data)) {
      // 发送失败，清理资源
      notify_data.clear();
    }
  }
}

auto KcpLoop::do_close_request(const service::NetEventData &event_data)
    -> void {
  auto it = channelMap_.find(event_data.close_request.channel_id);
  if (it != channelMap_.end()) {
    channelMap_.erase(it);
  }
  loop_->close(event_data.close_request.channel_id);
  // 发送到逻辑线程
  service::NetEventData notify_data{};
  notify_data.event_id = service::NetEvent::close_notify;
  notify_data.close_notify.channel_id = event_data.close_request.channel_id;
  if (!get_network()->get_net_queue().send(notify_data)) {
    // 发送失败，清理资源
    notify_data.clear();
  }
}

auto KcpLoop::on_accept(std::uint64_t channel_id, const std::string &name)
    -> void {
  // 发送到逻辑线程
  service::NetEventData event_data{};
  event_data.event_id = service::NetEvent::accept_notify;
  event_data.accept_notify.channel_id = channel_id;
  event_data.accept_notify.name = service::box_malloc(name.size() + 1);
  memcpy(event_data.accept_notify.name, name.c_str(), name.size() + 1);
  if (!get_network()->get_net_queue().send(event_data)) {
    // 内部错误或发送失败, 销毁资源
    loop_->close(channel_id);
    event_data.clear();
  } else {
    channelMap_[channel_id] =
        kratos::make_shared_pool_ptr<KcpChannel>(loop_.get(), channel_id);
  }
}

auto KcpLoop::on_receive(std::uint64_t channel_id, const char *data,
                         std::uint32_t size) -> void {
  auto it = channelMap_.find(channel_id);
  if (it == channelMap_.end()) {
    return;
  }
  // 输入到KCP
  ikcp_input(it->second->kcp_, data, (long)size);
  // 从KCP内返回可靠包
  auto bytes = ikcp_recv(
      it->second->kcp_, get_buffer_ptr(NETWORK_IOLOOP_UDP_RECV_BUFFER_MAX_SIZE),
      NETWORK_IOLOOP_UDP_RECV_BUFFER_MAX_SIZE);
  if (bytes <= 0) {
    return;
  }
  service::NetEventData event_data{};
  event_data.event_id = service::NetEvent::recv_data_notify;
  event_data.recv_data_notify.channel_id = channel_id;
  event_data.recv_data_notify.data_ptr = service::box_malloc(bytes);
  event_data.recv_data_notify.length = bytes;
  memcpy(event_data.recv_data_notify.data_ptr,
         get_buffer_ptr(NETWORK_IOLOOP_UDP_RECV_BUFFER_MAX_SIZE), bytes);
  // 发送到逻辑线程
  if (!get_network()->get_net_queue().send(event_data)) {
    event_data.clear();
    // 发送失败，清理资源
    loop_->close(channel_id);
    channelMap_.erase(it);
  }
}

auto KcpLoop::get_buffer_ptr(std::size_t length) -> char * {
  static auto buffer =
      std::make_unique<char[]>(NETWORK_IOLOOP_UDP_RECV_BUFFER_MAX_SIZE);
  static std::size_t buffer_length{NETWORK_IOLOOP_UDP_RECV_BUFFER_MAX_SIZE};
  if (!buffer || buffer_length < length) {
    buffer = std::make_unique<char[]>(length);
    buffer_length = length;
  }
  return buffer.get();
}

} // namespace loop
} // namespace kratos
