#include "call_tracer_zipkin_reporter.hh"
#include "config/box_config.hh"
#include "detail/http_base_impl.hh"
#include "http/http_call.hh"
#include "util/object_pool.hh"
#include "util/string_util.hh"
#include "util/time_util.hh"
#include "json/json.h"
#include <chrono>
#include <iostream>
#include <mutex>
#include <sstream>

namespace kratos {
namespace service {

CallTracerZipkinReport::CallTracerZipkinReport() {}

CallTracerZipkinReport::~CallTracerZipkinReport() {
  running_ = false;
  if (worker_.joinable()) {
    worker_.join();
  }
  http_ptr_->stop();
}

auto CallTracerZipkinReport::start(kratos::config::BoxConfig &config) -> bool {
  const static char *ATTR_NAME = "call_tracer.reporter.zipkin";
  if (config.has_attribute(ATTR_NAME)) {
    if (!util::get_host_config(config.get_string(ATTR_NAME), host_ip_,
                               host_port_)) {
      // TODO error
      return false;
    }
  }
  http_ptr_ = make_shared_pool_ptr<http::HttpBaseImpl>(nullptr);
  if (!http_ptr_->start()) {
    return false;
  }
  running_ = true;
  worker_ = std::thread([this]() {
    std::time_t ts = util::get_os_time_millionsecond();
    while (running_) {
      auto now = util::get_os_time_millionsecond();
      if (now - ts > 1000) {
        // 发送并清空队列日志
        flush_log();
        ts = now;
      }
      // 在工作线程内调用HTTP主循环
      http_ptr_->update(now);
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
    // 发送并清空队列日志
    flush_log();
    // TODO 这里会丢失关闭时的日志
  });
  return true;
}

auto CallTracerZipkinReport::trace(const TraceInfo &trace_info) -> void {
  log_queue_.enqueue(new TraceInfo(trace_info));
}

auto CallTracerZipkinReport::update(std::time_t /*now*/) -> void {}

auto CallTracerZipkinReport::flush_log() noexcept(true) -> void {
  //
  // 一次取出队列内所有信息
  //
  TraceInfo *log_ptr{nullptr};
  try {
    Json::Value array_value(Json::ValueType::arrayValue);
    while (log_queue_.try_dequeue(log_ptr)) {
      std::unique_ptr<TraceInfo> auto_deleter(log_ptr);
      report_log(array_value, log_ptr);
    }
    if (array_value.size()) {
      report_to_zipkin_server(array_value);
    }
  } catch (std::exception & /*ex*/) {
    // TODO error
  }
}

auto CallTracerZipkinReport::report_log(Json::Value &span,
                                        TraceInfo *log_raw_ptr) -> void {
  static const std::unordered_map<TraceType, std::string> kind_map = {
      {TraceType::NONE, ""},
      {TraceType::CLIENT, "CLIENT"},
      {TraceType::SERVER, "SERVER"},
      {TraceType::PRODUCER, "PRODUCER"},
      {TraceType::CONSUMER, "CONSUMER"},
  };
  static const std::unordered_map<TraceDetailType, std::string> detail_map = {
      {TraceDetailType::NONE, ""},
      {TraceDetailType::PROXY_CALL_REMOTE_SERVICE, "Proxy call remote service"},
      {TraceDetailType::PROXY_RECV_SERVICE_RESULT,
       "Proxy received service's result"},
      {TraceDetailType::SERVICE_RECV_PROXY_REQUEST,
       "Service received proxy's request"},
      {TraceDetailType::SERVICE_SEND_PROXY_RESULT,
       "Service sent result to proxy"},
  };
  //
  // log_raw_ptr在外部销毁
  //
  auto log_ptr = log_raw_ptr;
  // 目标格式为JSON格式, 每行日志为一个JSON对象
  // 参考: https://zipkin.io/zipkin-api/
  //
  Json::Value line;
  line["kind"] = kind_map.at(log_ptr->type);
  line["traceId"] = std::move(log_ptr->trace_id);
  line["id"] = std::move(std::to_string(log_ptr->span_id));
  if (log_ptr->parent_span_id) {
    line["parentId"] = std::move(std::to_string(log_ptr->parent_span_id));
  }
  line["name"] = std::move(log_ptr->service_name + "::" + log_ptr->method_name);
  Json::Value local_end_point;
  if (log_ptr->type == TraceType::CLIENT) {
    local_end_point["serviceName"] =
        std::move(log_ptr->service_name + " proxy");
  } else {
    local_end_point["serviceName"] = std::move(log_ptr->service_name);
  }
  local_end_point["ipv4"] = std::move(log_ptr->from_ip);
  local_end_point["port"] = log_ptr->from_port;
  line["localEndpoint"] = std::move(local_end_point);
  Json::Value remote_end_point;
  if (log_ptr->type == TraceType::CLIENT) {
    remote_end_point["serviceName"] = std::move(log_ptr->service_name);
  } else {
    remote_end_point["serviceName"] =
        std::move(log_ptr->service_name + " proxy");
  }
  remote_end_point["ipv4"] = std::move(log_ptr->target_ip);
  remote_end_point["port"] = log_ptr->target_port;
  line["remoteEndpoint"] = std::move(remote_end_point);
  line["timestamp"] = log_ptr->timestamp_ms;
  line["@timestamp"] = log_ptr->timestamp_ms;
  line["timestamp_millis"] = log_ptr->timestamp_ms / std::time_t(1000);
  line["duration"] = log_ptr->duration;
  line["shared"] = true;

#if defined(DEBUG) || defined(_DEBUG)
  Json::Value tags;
  tags["os.pid"] = log_ptr->pid;
  tags["method.args"] = std::move(log_ptr->param_info);
  line["tags"] = std::move(tags);
  Json::Value annotations;
  annotations["timestamp"] = log_ptr->timestamp_ms;
  annotations["value"] = detail_map.at(log_ptr->detail_type);
  Json::Value annotations_array(Json::ValueType::arrayValue);
  annotations_array.append(std::move(annotations));
  line["annotations"] = std::move(annotations_array);
#endif // defined(DEBUG) || defined(_DEBUG)

  span.append(std::move(line));
}

auto CallTracerZipkinReport::report_to_zipkin_server(Json::Value &span)
    -> void {
  static Json::StreamWriterBuilder builder;
  static std::unique_ptr<Json::StreamWriter> writer(builder.newStreamWriter());
  std::stringstream oss;
  // 写入流
  writer->write(span, &oss);
  // 发送到远程HTTP服务
  http_ptr_->do_request_async(host_ip_,        // ZIPKIN服务地址
                              host_port_,      // ZIPKIN服务端口
                              "/api/v2/spans", // API目录
                              "POST",          // HTTP POST方法
                              {},              // HTTP header
                              oss.str()        // 流
  );
}

} // namespace service
} // namespace kratos
