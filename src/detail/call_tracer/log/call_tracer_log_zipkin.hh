#pragma once

#include "call_tracer/call_tracer.hh"
#include "util/spsc_queue.hpp"
#include "util/time_system.hh"

#include <ctime>
#include <fstream>
#include <memory>
#include <thread>

namespace Json {
class Value;
class StreamWriterBuilder;
class StreamWriter;
} // namespace Json

namespace kratos {
namespace service {

using JsonSWBuilderPtr = std::unique_ptr<Json::StreamWriterBuilder>;
using JsonStreamWriterPtr = std::unique_ptr<Json::StreamWriter>;

/**
 * 跟踪日志文件写入.
 */
class CallTracerLogZipkin : public CallTracer {
  corelib::SPSCQueue<TraceInfo *> log_queue_; ///< 日志队列
  std::thread worker_;                        ///< 写入文件的工作线程
  std::ofstream cur_ofs_;                     ///< 当前写入文件流
  bool running_{false};                       ///< 运行标志
  time_system::Datetime last_date_;           ///< 最新的日期时间戳
  std::string log_root_dir_;     ///< 日志文件存放的根目录
  std::int32_t pid_{0};          ///< 当前进程ID
  JsonSWBuilderPtr swbuilder_;   ///< Json Stream builder
  JsonStreamWriterPtr swwriter_; ///< Json Stream writer

public:
  CallTracerLogZipkin();
  virtual ~CallTracerLogZipkin();
  virtual auto start(kratos::config::BoxConfig &config) -> bool override;
  virtual auto trace(const TraceInfo &trace_info) -> void override;
  virtual auto update(std::time_t now) -> void override;

private:
  /**
   * 从队列内取出日志写入日志文件.
   *
   * \return
   */
  auto run() noexcept(true) -> void;
  /**
   * 检查是否切换日志文件.
   *
   * \param stamp_sec 当前的时间戳，秒
   * \return true 没有错误发生, false 发生错误
   */
  auto check_and_switch(std::time_t stamp_sec) noexcept(true) -> bool;
  /**
   * 根据当前日期建立一个新的日志文件，关闭当前的日志文件(如果有).
   *
   * \param date_time 日期
   * \return true 没有错误发生, false 发生错误
   */
  auto new_log_file(const time_system::Datetime &date_time) noexcept(true)
      -> bool;
  /**
   * 将队列内所有日志写入日志文件.
   *
   * \return
   */
  auto flush_log() noexcept(true) -> void;
  /**
   * 写入一条调用日志.
   *
   * \param array_value 数组
   * \param call_log_ptr 调用日志指针
   * \return
   */
  auto write_and_destroy_log(TraceInfo *&log_ptr) noexcept(false) -> void;
};

} // namespace service
} // namespace kratos
