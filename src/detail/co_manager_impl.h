﻿#pragma once

#include "co_manager/co_manager.h"
#include "util/box_std_allocator.hh"
#include "util/object_pool.hh"

namespace kratos {
namespace service {

class ServiceContext;
class ServiceContextImpl;

/**
 * 协程管理器实现类.
 */
class CoManagerImpl : public CoManager {
  ServiceContextImpl *ctx_{nullptr}; ///< 上下文
  using CoIDSet = kratos::service::PoolUnorderedSet<coroutine::CoroID>;
  CoIDSet co_id_set_; ///< 协程ID集合, 用于卸载清理

public:
  /**
   * 构造.
   *
   * \param ctx 上下文
   */
  CoManagerImpl(ServiceContext *ctx);
  /**
   * 析构.
   *
   */
  virtual ~CoManagerImpl();
  /**
   * 产生一个协程.
   *
   * \param co_func 协程函数
   * \return 协程ID
   */
  virtual auto spawn(std::function<void(void)> co_func)
      -> coroutine::CoroID override;
  /**
   * 管理一个协程.
   *
   * \param co_id 协程ID
   * \return
   */
  virtual auto close(coroutine::CoroID co_id) -> void override;
};

} // namespace service
} // namespace kratos
