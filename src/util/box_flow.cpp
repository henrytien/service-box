#include "box_flow.hh"
#include "box/service_box.hh"

kratos::util::BoxFlow::BoxFlow(BoxFlowManager *manager,
                               const std::string &description) {
  manager_ptr_ = manager;
  description_ = description;
}

kratos::util::BoxFlow::~BoxFlow() {}

auto kratos::util::BoxFlow::on_start() -> FlowStatus {
  if (!start_flow_func_) {
    return FlowStatus::SKIP;
  }
  return start_flow_func_();
}

auto kratos::util::BoxFlow::on_stop() -> FlowStatus {
  if (!stop_flow_func_) {
    return FlowStatus::SKIP;
  }
  return stop_flow_func_();
}

auto kratos::util::BoxFlow::on_update(std::time_t now) -> FlowStatus {
  if (!tick_flow_func_) {
    return FlowStatus::SKIP;
  }
  return tick_flow_func_(now);
}

auto kratos::util::BoxFlow::start(StartFlowFunc flow_func) -> BoxFlow & {
  start_flow_func_ = flow_func;
  return *this;
}

auto kratos::util::BoxFlow::stop(StopFlowFunc flow_func) -> BoxFlow & {
  stop_flow_func_ = flow_func;
  return *this;
}

auto kratos::util::BoxFlow::update(TickFlowFunc flow_func) -> BoxFlow & {
  tick_flow_func_ = flow_func;
  return *this;
}

auto kratos::util::BoxFlow::add_start() -> BoxFlow & {
  manager_ptr_->add_start(this);
  return *this;
}

auto kratos::util::BoxFlow::add_term() -> BoxFlow & {
  manager_ptr_->add_term(this);
  return *this;
}

auto kratos::util::BoxFlow::add_update() -> BoxFlow & {
  manager_ptr_->add_update(this);
  return *this;
}

auto kratos::util::BoxFlow::get_description() -> const std::string & {
  return description_;
}

auto kratos::util::BoxFlow::set_last_update_status(FlowStatus update_status)
    -> void {
  last_update_status_ = update_status;
}

auto kratos::util::BoxFlow::get_last_update_status() -> FlowStatus {
  return last_update_status_;
}

kratos::util::BoxFlowManager::BoxFlowManager() {}

kratos::util::BoxFlowManager::~BoxFlowManager() {}

auto kratos::util::BoxFlowManager::add_start(BoxFlow *module_ptr)
    -> BoxFlowManager & {
  startup_seq_.push_back(module_ptr);
  return *this;
}

auto kratos::util::BoxFlowManager::add_term(BoxFlow *module_ptr)
    -> BoxFlowManager & {
  term_seq_.push_back(module_ptr);
  return *this;
}

auto kratos::util::BoxFlowManager::add_update(BoxFlow *module_ptr)
    -> BoxFlowManager & {
  update_seq_.push_back(module_ptr);
  return *this;
}

auto kratos::util::BoxFlowManager::new_flow(const std::string &description)
    -> BoxFlowPtr {
  auto ptr = make_shared_pool_ptr<BoxFlow>(this, description);
  all_.push_back(ptr);
  return ptr;
}

auto kratos::util::BoxFlowManager::start(service::ServiceBox *box) -> bool {
  for (auto &m : startup_seq_) {
    std::string flow_info = "[flow]" + m->get_description();
    auto result = m->on_start();
    if (FlowStatus::FAILURE == result) {
      box->write_log_line(klogger::Logger::INFORMATION,
                          flow_info + "...failed");
      return false;
    } else if (FlowStatus::SKIP == result) {
      box->write_log_line(klogger::Logger::INFORMATION,
                          flow_info + "...skipped");
    } else {
      box->write_log_line(klogger::Logger::INFORMATION, flow_info + "...done");
    }
  }
  return true;
}

auto kratos::util::BoxFlowManager::stop(service::ServiceBox *box) -> void {
  for (auto &m : term_seq_) {
    std::string flow_info = "[flow]" + m->get_description();
    auto result = m->on_stop();
    if (FlowStatus::FAILURE == result) {
      box->write_log_line(klogger::Logger::INFORMATION,
                          flow_info + "...failed");
    } else if (FlowStatus::SKIP == result) {
      box->write_log_line(klogger::Logger::INFORMATION,
                          flow_info + "...skipped");
    } else {
      box->write_log_line(klogger::Logger::INFORMATION, flow_info + "...done");
    }
  }
}

auto kratos::util::BoxFlowManager::update(std::time_t now) -> void {
  for (auto &m : update_seq_) {
    m->set_last_update_status(m->on_update(now));
  }
}

auto kratos::util::BoxFlowManager::print_update_flow() -> std::string {
  std::string result = "\n";
  for (auto &m : update_seq_) {
    std::string status_str;
    // ��ȡupdate״̬����ӡ
    if (FlowStatus::SKIP == m->get_last_update_status()) {
      result += "  - " + m->get_description() + "...skipped\n";
    } else {
      result += "  - " + m->get_description() + "\n";
    }
  }
  return result;
}
