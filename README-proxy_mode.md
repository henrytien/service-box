# service box proxy

service box可以以代理模式启动，启动方式如下(假设以默认容器为例):

```
 ./box -c box.cfg --proxy
```

# 代理模式下调用关系

![](https://images.gitee.com/uploads/images/2020/1211/110609_e6bf72dd_467198.png "屏幕截图.png")

## proxy模式的功能
1. 提供外部（集群外部）连接到集群内部的网关功能
2. 提供对外的服务发现入口，即任何连接的客户端都不需要自己进行服务发现，proxy将根据调用RPC协议自动在集群内部查找服务并自动建立连接并作为调用的转发器
3. 客户端只要连接到任何一个proxy即可以进行与集群内部服务实例的RPC通信，对于客户端来说大大简化了功能开发的过程

## proxy的配置

[配置说明](README-configuration.md)

